package lab3.minesweeper_1;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    public GridView grid;
    //Initial Value of array to be filled
    static String[] items = new String[] {
            "A", "B", "C", "D", "E",
            "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "O",
            "P", "Q", "R", "S", "T",
            "U", "V", "W", "X", "Y", "Z",
            "A", "C","","D","",
            "K","G","H","","J",
            "K","l","M","N","O",
            "P","","R","","T",
            "U","V","W","X","Y",
            "Z", "K", "L", "M", "N", "O",
            "K","","","J","L", "M",
            "N", "O", "K", "L", "M", "N", "O",
            "P", "Q", "R", "S", "T", "R", "S",
            "T", "Z", "A", "B"};
    String itemToBeChanged = "";
    //On load method
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //9 * 9 grid
        grid = (GridView) this.findViewById(R.id.minesweeper);
        final CustomGridAdapter customGrid = new CustomGridAdapter(MainActivity.this, items);
        grid.setAdapter(customGrid);
        //Grid click handlers
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView textView = (TextView) view;

                textView.setTextColor(Color.GREEN);

                textView.setBackgroundColor(Color.BLACK) ;

                TextView textView1 = (TextView) view.findViewById(R.id.cellId);

                textView1.setText(itemToBeChanged);
            }
        });
        //Button Event to generate/modify the value for the grid cell
        Button A = (Button) findViewById(R.id.btnA);

        A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                itemToBeChanged = "A";
            }
        });

        Button B = (Button) findViewById(R.id.btnB);

        B.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                itemToBeChanged = "B";
            }
        });

        Button C = (Button) findViewById(R.id.btnC);

        C.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                itemToBeChanged = "C";
            }
        });

        Button D = (Button) findViewById(R.id.btnD);

        D.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemToBeChanged = "D";
            }
        });

        Button E = (Button) findViewById(R.id.btnE);

        E.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemToBeChanged = "E";
            }
        });

        Button F = (Button) findViewById(R.id.btnF);

        F.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemToBeChanged = "F";
            }
        });

        Button G = (Button) findViewById(R.id.btnG);

        G.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemToBeChanged = "G";
            }
        });

        Button H = (Button) findViewById(R.id.btnH);

        H.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemToBeChanged = "H";
            }
        });

        Button I = (Button) findViewById(R.id.btnI);

        I.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemToBeChanged = "I";
            }
        });

        Button sort = (Button) findViewById(R.id.btnSort);
        //sorting logic
        sort.setOnClickListener(new View.OnClickListener() {
            String sortedItem[] = new String[81];
            String temp[] = new String[9];
            int count = 0, tempCount = 0;
            @Override
            public void onClick(View view) {
                for(int i=1; i<= grid.getChildCount();i++) {
                    TextView view2 = (TextView) grid.getChildAt(i - 1);
                    String item = view2.getText().toString();
                    //Logic to fill the empty spot
                    if(item == "" || item == null) {
                        view2.setText(getRandomValue());
                        item = view2.getText().toString();
                    }
                    temp[tempCount] = item;
                    tempCount++;
                    if(i % 9 == 0) {
                        Arrays.sort(temp);
                        tempCount = 0;

                        for (int r = 0; r < temp.length; r++) {
                            sortedItem[count] = temp[r];
                            count++;
                        }
                        temp = new String[9];
                        count = i;
                    }
                }
                //Refreshes the grid with the sorted value
                items = sortedItem;
                sortedItem = new String[81];
                count = 0;
                grid.setAdapter(new CustomGridAdapter(MainActivity.this, items));
                grid.invalidate();
            }
        });
    }
    //Generates the random value for the empty spot
    private String getRandomValue(){
        String[] alphabet = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I"};
        Random rand = new Random();
        int randomValue = rand.nextInt(alphabet.length);
        return alphabet[randomValue];
    }
}
