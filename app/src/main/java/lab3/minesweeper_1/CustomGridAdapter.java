package lab3.minesweeper_1;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by asok on 9/29/2017.
 */

public class CustomGridAdapter extends BaseAdapter {
    private Context context;
    private String[] items;
    LayoutInflater inflater;

    //constructor
    public CustomGridAdapter(Context context, String[] items) {
        this.context = context;
        this.items = items;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.cell, null);
        }

        TextView tv = (TextView) convertView.findViewById(R.id.cellId);
        //TextView tv = new TextView(this.context);
        tv.setText(items[position]);


        return tv;
    }
}
